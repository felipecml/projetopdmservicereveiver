package com.example.services;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class MainActivity extends ActionBarActivity {

	private Handler hand = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Object path = msg.obj;
			if (msg.arg1 == RESULT_OK && path != null) {
				Toast.makeText(MainActivity.this,
						getString(R.string.download_success, path.toString()),
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(MainActivity.this,
						getString(R.string.download_error), Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final Button startButton = (Button) findViewById(R.id.start_button);
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(MainActivity.this,DownloadService.class);
				Messenger messenger = new Messenger(hand);
				intent.putExtra("messenger", messenger);
				intent.setData(Uri.parse("cursos.html"));
				intent.putExtra("urlPath", "http://k19.com.br/cursos");
				startService(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
